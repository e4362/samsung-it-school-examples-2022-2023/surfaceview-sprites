package ru.dolbak.surfaceview;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import androidx.annotation.NonNull;

import java.util.ArrayList;

public class MySurface extends SurfaceView implements SurfaceHolder.Callback {

    float x, y; //текущее положение картинки
    float wi, hi; // размеры картинки
    float w, h; //размеры холста
    float tx, ty; //точки касания
    float dx, dy; // смещение картинки
    float koeff; //коэффициент скорости

    float xs[], ys[];
    int pointer = 0;
    int points = 2000;

    Bitmap image;
    Resources res;
    Paint paint;

    DrawThread drawThr;

    GameMap gameMap;
    boolean isFirst = true;

    ArrayList<Sprites> spritesArray = new ArrayList<Sprites>();

    public MySurface(Context context) {
        super(context);
        xs = new float[points];
        ys = new float[points];
        x = 100;
        y = 100;
        koeff = 20;
        res = getResources();
        image = BitmapFactory.decodeResource(res, R.drawable.sprites);
        hi = image.getHeight();
        wi = image.getWidth();
        paint = new Paint();
        getHolder().addCallback(this);

        spritesArray.add(new Sprites(this, image, 100, 100, 4));
        spritesArray.add(new Sprites(this, image, 400, 100, 5));
        spritesArray.add(new Sprites(this, image, 700, 100, 6));
        spritesArray.add(new Sprites(this, image, 100, 600, 7));

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN){
            tx = event.getX();
            ty = event.getY();
            calculate();
        }
        return true;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (isFirst){
            isFirst = false;
            h = canvas.getHeight();
            w = canvas.getWidth();
//            gameMap = new GameMap(w, h, res);
        }
        canvas.drawColor(Color.WHITE);
        for (int i = 0; i < spritesArray.size(); i++){
            spritesArray.get(i).draw(canvas);
        }
//        gameMap.draw(canvas);
//        for (int i = 0; i < points - 1; i++){
//            if (xs[i] == 0 && ys[i] == 0 || xs[i + 1] == 0 && ys[i + 1] == 0){
//                continue;
//            }
//            //canvas.drawCircle(xs[i], ys[i], 5, paint);
//            canvas.drawLine(xs[i], ys[i], xs[i+1], ys[i+1], paint);
//
//
//        }
//        w = canvas.getWidth();
//        h = canvas.getHeight();
//        canvas.drawBitmap(image, x, y, paint);
//        xs[pointer] = x;
//        ys[pointer] = y;
//        pointer += 1;
//        pointer %= points;
//        if (tx != 0){
//            calculate();
//        }
//        if (Math.abs(tx - x) < 10 || Math.abs(ty - y) < 10){
//            return;
//        }
//        x += dx;
//        y += dy;
//        checkScreen();
    }

    void checkScreen(){
        if (y + hi >= h || y <= 0){
            dy = -dy;
        }
        if (x + wi >= w || x <= 0){
            dx = -dx;
        }
    }

    private void calculate(){
        double g = Math.sqrt((tx-x)*(tx-x)+(ty-y)*(ty-y));
        dx = (float) (koeff * (tx-x)/g);
        dy = (float) (koeff * (ty-y)/g);
    }

    @Override
    public void surfaceCreated(@NonNull SurfaceHolder surfaceHolder) {
        drawThr = new DrawThread(this, getHolder());
        drawThr.setRun(true);
        drawThr.start();
    }

    @Override
    public void surfaceChanged(@NonNull SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(@NonNull SurfaceHolder surfaceHolder) {
        boolean stop = true;
        drawThr.setRun(false);
        while (stop){
            try{
                drawThr.join();
                stop = false;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
