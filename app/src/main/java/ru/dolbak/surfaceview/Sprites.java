package ru.dolbak.surfaceview;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public class Sprites {
    MySurface mySurface;
    Bitmap image;
    int x, y;
    float dx, dy;
    int height, width;
    Paint paint = new Paint();
    final int IMAGE_COLUMNS = 10;
    final int IMAGE_ROWS = 8;
    int currentFrame = 0;
    int character;

    public Sprites(MySurface mySurface, Bitmap image, int x, int y, int character) {
        this.mySurface = mySurface;
        this.image = image;
        width = this.image.getWidth() / IMAGE_COLUMNS;
        height = this.image.getHeight() / IMAGE_ROWS;
        this.x = x;
        this.y = y;
        this.character = character;
    }

    public void controlRoute(){
        if (x < 0 || x > mySurface.getWidth() - image.getWidth()){
            dx = -dx;
        }
        if (y < 0 || y > mySurface.getHeight() - image.getHeight()){
            dy = -dy;
        }
        x += dx;
        y += dy;
        currentFrame = ++currentFrame%IMAGE_COLUMNS;
    }

    public void setSpeed(float sX, float sY){
        dx = sX;
        dy = sY;
    }

    public void draw(Canvas canvas){
        controlRoute();
        Rect src = new Rect(currentFrame*width, character*height,
                currentFrame*width+width, character*height+height);
//        Rect src = new Rect(character*width, currentFrame*height,
//                character*width+width, currentFrame*height+height);
        Rect dst = new Rect(x, y, x+width, y + height);
        canvas.drawBitmap(image, src, dst, paint);
    }
    public void setX(int x){
        this.x = x;
    }
    public void setY(int y){
        this.y = y;
    }
}
